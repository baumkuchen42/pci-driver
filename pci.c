/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <stdbool.h>
#include <linux/types.h>
#include <linux/poll.h>
#include <asm/io.h>
#include <linux/delay.h>

#define MY_VENDOR_ID 0x144a
#define MY_DEVICE_ID 0x9111


static unsigned long base_addr=0L, iolen=0L;

static dev_t mypci_dev_number;
static struct cdev *driver_object;
static struct class *mypci_class;
static struct device *mypci_dev;
static char driver_name[] = "pci";


static int pci_open(struct inode* inode, struct file* instance) {
	return 0;
}

static int pci_close(struct inode* inode, struct file* instance) {
	return 0;
}

static bool has_data(void) {
	u8 status;
	status = inb(base_addr + 8);
	return (status & 0b00010000) != 0;
}

static bool is_busy(void) {
	u8 status;
	status = inb(base_addr + 8);
	return (status & 0b10000000) == 0;
}

static void configure(void) {
	outb(0x00, base_addr + 8); // set region +- 10V
	outb(0x01, base_addr + 0x0a); // sw trigger mode
	outb(0x00, base_addr + 0x06); // choose channel 0
}

static ssize_t pci_read(struct file* instance, char __user* user, size_t count, loff_t* offset) {
	pr_info("%s: base address is %li", driver_name, base_addr);
	unsigned long not_copied, to_copy;
	u16 data = 0;
	int timeout = 100;
	const struct pci_dev* dev = instance->private_data;
	// erst leerlesen, dann irgendwas an SW-Trigger, dann fragen, ob BUSY, wenn nicht mehr, auslesen
	// ausgelesenen Wert umwandeln (siehe Manual) und an user
	while(has_data() && timeout != 0) {
		inw(base_addr);
		timeout --;
	}
	outb(0x00, base_addr + 14); // sw trigger
	while(is_busy() && timeout != 0) {
		msleep(10);
		timeout --;
	}
	data = inw(base_addr);
	
	to_copy = min(count, sizeof(data));
	not_copied = copy_to_user(user, &data, to_copy);
	*offset += to_copy - not_copied;
	
	return to_copy - not_copied;
}

static int pci_probe(struct pci_dev *pdev, const struct pci_device_id *id) {
	//pr_info("%s: probing, vendor: %hx, device: %hx", driver_name, MY_VENDOR_ID, MY_DEVICE_ID);
	if(pci_enable_device(pdev) != 0) {
		pr_err("%s: enabling failed", driver_name);
		return -EIO;
	}
	pr_info("%s: enabled device", driver_name);
	base_addr = pci_resource_start(pdev, 2);
	pr_info("%s: base addr is %i", driver_name, base_addr);
	iolen = pci_resource_len(pdev, 2);
	if(request_region(base_addr, iolen, pdev->dev.kobj.name) == NULL) {
		dev_err(&pdev->dev,"I/O address conflict for device \"%s\"\n", pdev->dev.kobj.name);
		return -EIO;
	}
	configure();
	return 0;
	cleanup_ports:
		release_region(base_addr, iolen);
		return -EIO;
}

static void device_deinit(struct pci_dev *pdev) {
	if(base_addr) {
		release_region(base_addr, iolen);
	}
}

static struct file_operations fops = {
	.read = pci_read,
	.open = pci_open,
	.release = pci_close,
};

static struct pci_device_id pci_drv_tbl[] = {
	{ MY_VENDOR_ID, MY_DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0 },
	{ 0, }
};

static struct pci_driver pci_drv = {
	.name= driver_name,
	.id_table= pci_drv_tbl,
	.probe= pci_probe,
	.remove= device_deinit,
};

static int __init mod_init(void)
{
	if(alloc_chrdev_region(&mypci_dev_number, 0, 1, driver_name) < 0) {
		return -EIO;
	}
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if(driver_object == NULL) {
		goto free_dev_number;
	}
	printk("%s: device number is %d", driver_name, mypci_dev_number);
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if(cdev_add(driver_object, mypci_dev_number, 1) != 0) {
		pr_err("%s: cdev add failed", driver_name);
		goto free_cdev;
	}
	/* Eintrag im Sysfs, damit Udev den Geraetedateieintrag erzeugt. */
	mypci_class = class_create(THIS_MODULE, driver_name);
	if(IS_ERR(mypci_class)) {
		pr_err("mypci: no udev support available\n");
		goto free_cdev;
	}
	mypci_dev = device_create(mypci_class, NULL, mypci_dev_number, NULL, "%s", driver_name);
	pr_info("%s: mypci_dev created", driver_name);

	int ret = 0;
	ret = pci_register_driver(&pci_drv);
	pr_info("%s: return value of pci registration: %i", driver_name, ret);
	if(ret < 0)  {
		device_destroy(mypci_class, mypci_dev_number);
		pr_err("%s: PCI subsystem registration failed", driver_name);
		goto free_dev_number;
	}
	pr_info("%s: init successful", driver_name);
	return 0;

	free_cdev:
		    kobject_put(&driver_object->kobj);
	free_dev_number:
		    unregister_chrdev_region(mypci_dev_number, 1);
		    return -EIO;
}

static void __exit mod_exit(void) {
	pci_unregister_driver(&pci_drv);

	device_destroy(mypci_class, mypci_dev_number);
	class_destroy(mypci_class);
	cdev_del(driver_object);
	unregister_chrdev_region(mypci_dev_number, 1);
	pr_info("%s: deregistered", driver_name);
}

module_init(mod_init);
module_exit(mod_exit);
MODULE_LICENSE("GPL");
